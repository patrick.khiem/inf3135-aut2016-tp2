# Travail pratique 2

## Description

Description du projet en quelques phrases.
Mentionner le contexte (cours, sigle, universit�, etc.).

## Auteurs

- Pr�nom et nom (Code permanent)
- Pr�nom et nom (Code permanent)
- Pr�nom et nom (Code permanent)

## Plateformes support�es

Indiquez ici la liste des plateformes sur lesquelles le projet a �t� test�
(MacOS, Ubuntu, Mint, Serveur Matl etc.). N'oubliez pas de pr�ciser la version
de la plateforme (MacOS 10.10.5 Yosemite, Ubuntu 14.04 LTS, etc.).

## D�pendances

Donnez la liste des biblioth�ques et des logiciels qui doivent �tre install�es
pour que le projet soit fonctionnel. Donnez le lien vers ceux-ci pour que
l'utilisateur puisse �tre redirig� vers les sites officiels des
logiciels/biblioth�ques pour effectuer l'installation le cas �ch�ant. N'oubliez
pas d'indiquer

## Installation

Expliquez comment rendre le projet fonctionnel (commandes make et make data ?)
et comment lancer la suite de tests automatiques (make test ?), comment
nettoyer le dossier (make clean ?).

## Fonctionnement

Expliquez bri�vement comment utiliser votre programme avec au moins trois
exemples d'utilisation (commande lanc�e et r�sultat affich�).  Assurez-vous que
les exemples de commande lanc�e et de r�sultats obtenus sont format�s
correctement � l'aide de Markdown.

## Contenu du projet

D�crivez bri�vement chacun des fichiers contenus dans le projet. Utilisez
une liste � puce et d�crivez-les de fa�on significative (une phrase par
fichier)

## R�f�rences

Citez vos sources ici, s'il y a lieu.

## Statut

Indiquez le statut actuel du projet et les bogues connus s'il y a lieu. Aussi,
utilisez cette section pour indiquer la liste des t�ches � effectuer pour
compl�ter le projet en utilisant le [format sp�cial
Markdown](https://docs.gitlab.com/ce/user/markdown.html#task-lists) :

- [ ] T�che 1

  - [x] Sous-t�che 1.1 (compl�t� par Alice)
  - [ ] Sous-t�che 1.2 (responsable : Bob)

- [ ] T�che 2

  - [ ] Sous-t�che 2.1 (responsable : Alice)
  - [x] Sous-t�che 2.2 (compl�t� par Bob)
  - [ ] Sous-t�che 2.3 (responsable : Alice et Bob)

- [ ] T�che 3
